/**
 * 
 */
package com.satispay.sollauth.client;

import lombok.Getter;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Getter
public enum RequestHeader {

	DATE("Date"),
	DIGEST("Digest"),
	HOST("Host"),
	CONTENTTYPE("Content-Type"),
	AUTHORIZATION("Authorization"),
	REQUEST_TARGET("(request-target)");
	
	private final String value;
	private RequestHeader(String value) {
		this.value = value;
	}
	
	
}
