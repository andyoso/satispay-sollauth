/**
 * 
 */
package com.satispay.sollauth.client;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.http.HttpStatus;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.satispay.sollauth.client.request.GenericRequest;
import com.satispay.sollauth.client.response.GenericResponse;
import com.satispay.sollauth.client.response.Outcome;
import com.satispay.sollauth.client.response.Outcome.Status;
import com.satispay.sollauth.exception.RestClientException;

import lombok.extern.log4j.Log4j2;

/**
 * Abstract class that implements the common methods for all the REST clients.
 * To create a new REST Client just extend this class implementing the costructor
 * and the method to retrieve the service URL ({@link getUrl()})
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Log4j2
@SuppressWarnings("rawtypes")
public abstract class AbstractRestClient<K extends GenericRequest<?>,T extends GenericResponse> implements Client<K, T> {

	private static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	private Class<T> responseClass;
	private static final Gson GSON = new Gson();
	protected HttpMethod method;

	public abstract String getUrl();

	/**
	 * The Client's costructor
	 * @param method - the HttpMethod used to invoke the service
	 */
	protected AbstractRestClient(HttpMethod method) {
		this.method = method;
		responseClass = getParamClass(getClass(), 1);
	}

	/**
	 * Internal method to retrieve the Class instance of a class' parameter
	 * @param clazz
	 * @param param the parameter number (position)
	 * @return
	 */
	private Class<T> getParamClass(Class<?> clazz,int param){
		try {
			Type t = clazz.getGenericSuperclass();
			return (Class<T>)((ParameterizedType)t).getActualTypeArguments()[param];
		}catch(ClassCastException e) {
			return getParamClass(clazz.getSuperclass(),param);
		}
	}

	/**
	 * This method contains the actions to do before the http request
	 * @param request
	 * @param httpRequest
	 * @throws Exception
	 */
	protected void doBefore(K request,HttpRequest httpRequest) throws Exception {
		//DO NOTHING
	}

	/**
	 * This method contains the actions to do after the http request
	 * @param request
	 * @throws Exception
	 */
	protected void doAfter(HttpResponse<String> request) throws Exception {
		//DO NOTHING!
	}

	/**
	 * Prepare and execute the request to the remote service
	 * @param request - the service's request
	 * @return an object rappresentation of the service's response
	 */
	@Override
	public final T call(K request) throws RestClientException {
		final HttpRequest httpRequest;
		final HttpResponse<String> httpResponse;
		try {
			httpRequest  = initHttpRequest(request);
			doBefore(request,httpRequest);
			logData(httpRequest,request);
			httpResponse = httpRequest.asString();
			doAfter(httpResponse);
		}catch(Exception e) {
			String msg = "An error occurred while invoking the service";
			log.error(msg);
			throw new RestClientException(msg,e);
		}
		return getResponse(httpResponse);

	}
	private T getResponse(HttpResponse<String> httpResponse) throws RestClientException  {
		try {
			log.info("RESPONSE : {}",httpResponse.getBody());
			Constructor constructor = responseClass.getConstructor();
			T response = (T)constructor.newInstance();
			if(httpResponse.getStatus()==HttpStatus.OK.value()) {
				response.setOutcome(new Outcome(Status.OK, null));
				response.getHeaders().putAll(httpResponse.getHeaders());			 
				response.setBody(GSON.fromJson(httpResponse.getBody(), response.getBodyClazz()));
			}else {
				response.setOutcome(new Outcome(Status.KO, httpResponse.getBody()));
				response.getHeaders().putAll(httpResponse.getHeaders());			 
			}			
			return response;
		}catch(Exception e) {
			String msg = "An error occurred while parsing the response";
			throw new RestClientException(msg,e);
		}

	}

	/**
	 * Initialize the HttpRequest object, populating it with the basic minimum headers and the body\queryStrings
	 * @param request
	 * @return
	 * @throws URISyntaxException
	 */
	private HttpRequest initHttpRequest(K request) throws URISyntaxException {
		final HttpRequest httpRequest;
		if(method==HttpMethod.HEAD || method==HttpMethod.GET) {
			GetRequest gr = new GetRequest(method, getUrl());
			gr.queryString(getBodyAsMap(request.getBody()));
			httpRequest = gr;

		}else {
			HttpRequestWithBody br = new HttpRequestWithBody(method, getUrl());
			br.body(GSON.toJson(request.getBody()));
			httpRequest = br;
		}

		httpRequest.header(RequestHeader.DATE.getValue(), 
				new SimpleDateFormat(DATEFORMAT).format(new Date()));

		httpRequest.header(RequestHeader.HOST.getValue(),
				getHost());

		httpRequest.header(RequestHeader.CONTENTTYPE.getValue(),
				"application/json");

		request.getHeaders()
		.entrySet()
		.stream()
		.forEach(e -> httpRequest.header(e.getKey(), GSON.toJson(e.getValue())));

		return httpRequest;
	}

	private Map<String,Object> getBodyAsMap(Object o){
		String json = GSON.toJson(o);
		Type type = new TypeToken<Map<String, Object>>() {}.getType();
		return GSON.fromJson(json, type);

	}

	private void logData(HttpRequest httpRequest,K request) {
		log.info("URL : {}",httpRequest.getUrl());
		log.info("METHOD: {}",method);
		String h = httpRequest.getHeaders()
				.entrySet()
				.stream()
				.map(e -> e.getKey()+":"+StringUtils.join(e.getValue(),' '))
				.reduce("", (a,b) -> a+"\n"+b);
		log.info("HEADERS: {}",h);

		log.info("BODY: {}",GSON.toJson(request.getBody()));
	}

	private String getHost() throws URISyntaxException {
		URI uri = new URI(getUrl());
		return uri.getHost();
	}
}
