/**
 * 
 */
package com.satispay.sollauth.client;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
public interface Client<K,T> {
	T call(K request) throws Exception;
}
