/**
 * 
 */
package com.satispay.sollauth.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.mashape.unirest.request.HttpRequest;
import com.satispay.sollauth.cypher.Cypher;
import com.satispay.sollauth.exception.SigningException;

import lombok.extern.log4j.Log4j2;

/**
 * This class provide a simple interface to create the signature and
 * generate the Authorization Header. To create signature string it uses {@link com.satispay.sollauth.cypher.Cypher}
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@Component
@Log4j2
public class RequestSigner {

	@Value("${auth.key.id}")
	private String keyId;
	@Autowired
	private Cypher cypher;
	@Autowired
	private Environment env;
	private PrivateKey privateKey;

	/**
	 * Just initialize the Signer with the private key token from properties file
	 * @throws URISyntaxException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws IOException
	 */
	@PostConstruct
	public void init() throws URISyntaxException, NoSuchAlgorithmException, InvalidKeySpecException, IOException  {
		String pkFilename = env.getProperty("auth.key.private");
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		Path path = Paths.get(classloader.getResource(pkFilename).toURI());
		privateKey = KeyReader.getPrivateKey(path);
	}


	/**
	 * Generate the Authorization Header from the input headers
	 * @param request - the httpRequest that contains the headers
	 * @param signHeaders - the list of the name of the headers to sign
	 * @return the authorization header
	 * @throws SigningException
	 */
	public String generateAuthorizationHeader(HttpRequest request,List<String> signHeaders) throws SigningException {
		List<String> headers = new ArrayList<>();
		try {
			for(String signHeader : signHeaders) {
				if(StringUtils.equals(RequestHeader.REQUEST_TARGET.getValue(), signHeader)) {
					headers.add(getRequestTarget(request));
				}else {
					headers.add(getHeaderAsString(request, signHeader));
				}
			}
			String toSign = StringUtils.join(headers,StringUtils.LF);
			log.debug("String to sign -> "+toSign);
			String signature = cypher.encrypt(toSign, privateKey);	
			log.debug("Signature -> "+signature);
			return buildAuthorizationHeader(signature, signHeaders);
		}
		catch(Exception ex) {
			String msg = "An error occurred while signing the http request";
			log.error(msg,ex);
			throw new SigningException(msg,ex);

		}
	}

	private String getRequestTarget(HttpRequest request) throws URISyntaxException {
		StringBuilder sb = new StringBuilder();
		sb.append(RequestHeader.REQUEST_TARGET.getValue())
			.append(": ")
			.append(request.getHttpMethod().name().toLowerCase())
			.append(" ")
			.append(getPath(request));
		return sb.toString();
	}

	private String getHeaderAsString(HttpRequest request,String headerName) {
		List<String> values = request.getHeaders().get(headerName);
		if(values == null || values.isEmpty()) {
			throw new IllegalArgumentException("The header '"+headerName+"' is empty or null");
		}else {
			return getKeyValueAsString(headerName, StringUtils.join(values, ","));
		}
	}
	
	private String getKeyValueAsString(String key,Object value) {
		return new StringBuilder()
			.append(key)
			.append(": ")
			.append(value)
			.toString();
	}
	
	private String buildAuthorizationHeader(String signature,List<String> signHeaders) {
		return new AuthHeaderBuilder()
				.keyId(keyId)
				.signature(signature)
				.headers(signHeaders)
				.build();
	}

	private String getPath(HttpRequest request) throws URISyntaxException {
		URI uri = new URI(request.getUrl());
		if(uri.getQuery()==null || uri.getQuery().isEmpty()) {
			return uri.getPath();
		}
		return uri.getPath()+'?'+uri.getQuery();
	}

}
