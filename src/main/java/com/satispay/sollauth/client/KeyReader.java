/**
 * 
 */
package com.satispay.sollauth.client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

/**
 * This class allows to extract the private key from PEM file
 * @author Andrea Solla
 * @date 9 mag 2021
 */
public class KeyReader {
	
	private static final String BEGIN_PK = "-----BEGIN PRIVATE KEY-----";
	private static final String END_PK = "-----END PRIVATE KEY-----";
	
	private KeyReader() {}
	
	/**
	 * This method create a PrivateKey object from the file stored in the input Path
	 * @param file the path of the PEM file
	 * @return the object rappresentation of the private key
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws InvalidKeySpecException
	 */
	public static PrivateKey getPrivateKey(Path file) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
		StringBuilder sb = new StringBuilder();
		sb.append(new String(Files.readAllBytes(file)));
		sb.delete(sb.indexOf(BEGIN_PK),sb.indexOf(BEGIN_PK)+BEGIN_PK.length());
		sb.delete(sb.indexOf(END_PK),sb.indexOf(END_PK)+END_PK.length());
		int index = sb.indexOf("\n");
		while(index >=0) {
			sb.deleteCharAt(index);
			index = sb.indexOf("\n");
		}
		byte[] b1 = Base64.getDecoder().decode(sb.toString());
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
	}
}
