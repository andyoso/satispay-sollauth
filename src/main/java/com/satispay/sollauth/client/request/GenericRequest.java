/**
 * 
 */
package com.satispay.sollauth.client.request;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Data
public abstract class GenericRequest<T> implements Serializable{

	private static final long serialVersionUID = 1L;
	protected Map<String,Object> headers;
	protected T body;
} 
