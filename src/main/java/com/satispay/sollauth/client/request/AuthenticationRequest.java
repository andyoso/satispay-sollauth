/**
 * 
 */
package com.satispay.sollauth.client.request;

/**
 * @author Andrea Solla
 * @date 10 mag 2021
 */
public class AuthenticationRequest extends GenericRequest<AuthenticationRequestBody>{

	private static final long serialVersionUID = 1L;

}
