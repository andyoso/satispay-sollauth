/**
 * 
 */
package com.satispay.sollauth.client.request;

import java.io.Serializable;

import lombok.Data;

/**
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@Data
public class AuthenticationRequestBody implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String dummyString;
	

}
