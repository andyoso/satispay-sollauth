/**
 * 
 */
package com.satispay.sollauth.client;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.request.HttpRequest;
import com.satispay.sollauth.client.request.AuthenticationRequest;
import com.satispay.sollauth.client.response.AuthenticationResponse;
import com.satispay.sollauth.cypher.Cypher;

/**
 * Implementation of {@link 
com.satispay.sollauth.client.AbstractRestClient} that allows the invokation of Satispay's 
Authorization Gateway
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@Component
@Primary
public class GETAuthClient extends AbstractRestClient<AuthenticationRequest,AuthenticationResponse > {

	@Autowired
	private Cypher cypher;
	@Autowired
	private RequestSigner signer;
	@Value("${auth.url}")
	private String url;
	@Value("${auth.sign.headers}")
	private List<String> signHeaders;

	protected GETAuthClient(HttpMethod met) {
		super(met);
	}
	public GETAuthClient() {
		super(HttpMethod.GET);
	}

	@Override
	protected void doBefore(AuthenticationRequest request, HttpRequest httpRequest) throws Exception {
		httpRequest.header(RequestHeader.DIGEST.getValue(), 
				cypher.digest(method==HttpMethod.GET? null : request.getBody()));
		if(signHeaders!=null && !signHeaders.isEmpty()) {
			httpRequest.header(RequestHeader.AUTHORIZATION.getValue(),
					signer.generateAuthorizationHeader(httpRequest, signHeaders));
		}
	}

	@Override
	public String getUrl() {
		return url;
	}


}
