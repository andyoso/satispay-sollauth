/**
 * 
 */
package com.satispay.sollauth.client;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.log4j.Log4j2;


/**
 * An 'utility' to generate the Authentication Headers
 * It just concatenate all the attributes used to generate the header
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@Log4j2
@SuppressWarnings("unused")
public class AuthHeaderBuilder {

	private String keyId;
	private String algorithm = "rsa-sha256";
	private String headers;
	private String signature;
	private String satispaysequence = "4";
	private String satispayperformance = "LOW";


	public AuthHeaderBuilder headers(List<String> value) {
		headers = StringUtils.join(value,StringUtils.SPACE);
		return this;
	}

	public AuthHeaderBuilder keyId(String value) {
		this.keyId = value;
		return this;
	}

	public AuthHeaderBuilder algorithm(String value) {
		this.algorithm = value;
		return this;
	}

	public AuthHeaderBuilder signature(String value) {
		this.signature = value;
		return this;
	}

	public AuthHeaderBuilder satispaysequence(String value) {
		this.satispaysequence = value;
		return this;
	}

	public AuthHeaderBuilder satispayperformance(String value) {
		this.satispayperformance = value;
		return this;
	}

	public String build()   {
		List<String> list =  Arrays.stream(this.getClass().getDeclaredFields())
				.map(this::addField)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		return "Signature "+StringUtils.join(list,", ");


	}

	private String addField(Field field) {
		try {
			StringBuilder sb = new StringBuilder();
			String fieldName = field.getName();
			Object fieldValue = field.get(this);
			if(fieldValue instanceof String) {
				sb.append(fieldName)
				.append("=")
				.append('"')
				.append(fieldValue)
				.append('"');
				return sb.toString();
			}
			return null;
		}catch(Exception e) {
			final String msg = "An error occurred while trying to add field to AuthHeader";
			log.error(msg,e);
			throw new IllegalArgumentException(msg,e);
		}		
	}


}
