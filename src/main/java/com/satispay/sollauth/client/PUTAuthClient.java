/**
 * 
 */
package com.satispay.sollauth.client;

import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpMethod;

/**
 * The alternative version of the {@link com.satispay.sollauth.client.GetAuthClient} 
 * to invoke the service with PUT method
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@Component
public class PUTAuthClient extends GETAuthClient {

	public PUTAuthClient() {
		super(HttpMethod.PUT);
	}
}
