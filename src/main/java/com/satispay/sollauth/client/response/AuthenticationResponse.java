/**
 * 
 */
package com.satispay.sollauth.client.response;

/**
 * @author Andrea Solla
 * @date 11 mag 2021
 */
public class AuthenticationResponse extends GenericResponse<AuthResponseBody> {

	private static final long serialVersionUID = 1L;

}
