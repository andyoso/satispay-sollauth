/**
 * 
 */
package com.satispay.sollauth.client.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Andrea Solla
 * @date 12 mag 2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Outcome {

	private Status status;
	private String errorMessage;
	
	public enum Status{ OK,KO }
}
