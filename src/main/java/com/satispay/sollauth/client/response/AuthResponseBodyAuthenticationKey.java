/**
 * 
 */
package com.satispay.sollauth.client.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Data
public class AuthResponseBodyAuthenticationKey {
	@SerializedName("access_key")
	private String accessKey;
	@SerializedName("customer_uid")
	private String customerUid;
	@SerializedName("key_type")
	private String keyType;
	@SerializedName("auth_type")
	private String authType;
	@SerializedName("role")
	private String role;
	@SerializedName("enable")
	private Boolean enable;
	@SerializedName("insert_date")
	private String insertDate;
	@SerializedName("update_date")
	private String updateDate;
	@SerializedName("version")
	private Float version;
}
