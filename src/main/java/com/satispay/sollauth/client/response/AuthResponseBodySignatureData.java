/**
 * 
 */
package com.satispay.sollauth.client.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Data
public class AuthResponseBodySignatureData {
	@SerializedName("key_id")
	private String keyId;
	@SerializedName("algorithm")
	private String algorithm;
	@SerializedName("headers")
	private List<String> headers;
	@SerializedName("signature")
	private String signature;
	@SerializedName("resign_required")
	private Boolean resignRequired;
	@SerializedName("iteration_count")
	private Float iterationCount;
	@SerializedName("valid")
	private Boolean valid;
}
