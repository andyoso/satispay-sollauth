/**
 * 
 */
package com.satispay.sollauth.client.response;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.Getter;

/**
 * @author Andrea Solla
 * @date 11 mag 2021
 */
@Data
public class GenericResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private Class<T> bodyClazz;
	protected Outcome outcome;
	protected Map<String,Object> headers;
	protected T body;
	
	
	public GenericResponse(){
		headers = new HashMap<>();
		bodyClazz = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass())
				.getActualTypeArguments()[0];
	}
}
