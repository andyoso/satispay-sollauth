/**
 * 
 */
package com.satispay.sollauth.client.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Data
public class AuthResponseBody {
	@SerializedName("authentication_key")
	private AuthResponseBodyAuthenticationKey authenticationKey;
	@SerializedName("signature")
	private AuthResponseBodySignatureData signature;
	@SerializedName("signed_string")
	private String signedString;
}
