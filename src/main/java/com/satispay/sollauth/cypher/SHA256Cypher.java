/**
 * 
 */
package com.satispay.sollauth.cypher;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Base64;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.satispay.sollauth.exception.EncryptionException;

import lombok.extern.log4j.Log4j2;

/**
 * Implementation of the {@link com.satispay.sollauth.cypher.Cypher} interface.
 * It provides some methods to encrypt data with SHA256 algorithm
 * @author Andrea Solla
 * @date 9 mag 2021
 */
@Log4j2
@Component
public class SHA256Cypher implements Cypher {

	private static final Gson GSON = new Gson();


	@Override
	public String encrypt(String input, PrivateKey privateKey) throws EncryptionException {
		log.info("Encrypting...");
		try{
			Signature privateSignature = Signature.getInstance("SHA256withRSA");
			privateSignature.initSign(privateKey);
			privateSignature.update(input.getBytes(StandardCharsets.UTF_8));
			byte[] s = privateSignature.sign();
			return Base64.getEncoder().encodeToString(s);
		}catch(Exception e){
			log.error("An error occurrend while trying to encrypt...",e);
			throw new EncryptionException("An error occurrend while trying to encrypt...",e);
		}
	}

	@Override
	public String digest(String input) throws NoSuchAlgorithmException {
		StringBuilder sb = new StringBuilder();
		if(input!=null) {
			sb.append("SHA-256=");
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
			sb.append(Base64.getEncoder().encodeToString(hash));
		}
		return sb.toString();
	}

	public String digest(Object input) throws NoSuchAlgorithmException {
		final String txt;
		if(input==null) {
			txt = null;
		}else {
			 txt = GSON.toJson(input);
		}
		return this.digest(txt);
	}


}
