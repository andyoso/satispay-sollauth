/**
 * 
 */
package com.satispay.sollauth.cypher;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import com.satispay.sollauth.exception.EncryptionException;

/**
 * Cypher interface
 * @author Andrea Solla
 * @date 9 mag 2021
 */
public interface Cypher {

	/**
	 * Encrypt the input string using the given private key
	 * @param input
	 * @param privateKey
	 * @return
	 * @throws EncryptionException
	 */
	String encrypt(String input,PrivateKey privateKey) throws EncryptionException;
	/**
	 * Create the digest of the input object
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	String digest(String input)  throws NoSuchAlgorithmException;
	/**
	 * Create the digest of the input object
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	String digest(Object input) throws NoSuchAlgorithmException;
}
