/**
 * 
 */
package com.satispay.sollauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.satispay.sollauth.client.response.AuthResponseBody;
import com.satispay.sollauth.exception.AuthenticationException;
import com.satispay.sollauth.service.AuthService;

/**
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@RestController
@RequestMapping("/")
public class AuthRestController {


	@Autowired
	private AuthService service;

	@GetMapping("/authenticate")
	public ResponseEntity<?> getAuthenticate() throws AuthenticationException{
		try {
			return new ResponseEntity<>(service.getAuthentication(),HttpStatus.OK);
		}catch(AuthenticationException e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/authenticate")
	public ResponseEntity<?> postAuthenticate() throws AuthenticationException{
		try {
			return new ResponseEntity<>(service.postAuthentication(),HttpStatus.OK);
		}catch(AuthenticationException e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/authenticate")
	public ResponseEntity<?> putAuthenticate() throws AuthenticationException{
		try {
			return new ResponseEntity<>(service.putAuthentication(),HttpStatus.OK);
		}catch(AuthenticationException e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/authenticate")
	public ResponseEntity<?> deleteAuthenticate() throws AuthenticationException{
		try {
			return new ResponseEntity<>(service.deleteAuthentication(),HttpStatus.OK);
		}catch(AuthenticationException e) {
			return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
