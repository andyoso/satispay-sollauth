/**
 * 
 */
package com.satispay.sollauth.service;

import com.satispay.sollauth.client.response.AuthResponseBody;
import com.satispay.sollauth.exception.AuthenticationException;

/**
 * @author Andrea Solla
 * @date 11 mag 2021
 */
public interface AuthService {

	/**
	 * This method execute a GET request to the Satispay Authentication service
	 * @return the object rappresentation of the service output
	 * @throws AuthenticationException
	 */
	AuthResponseBody getAuthentication()  throws AuthenticationException ;
	/**
	 * This method execute a PUT request to the Satispay Authentication service
	 * @return the object rappresentation of the service output
	 * @throws AuthenticationException
	 */
	AuthResponseBody putAuthentication()  throws AuthenticationException ;
	/**
	 * This method execute a POST request to the Satispay Authentication service
	 * @return the object rappresentation of the service output
	 * @throws AuthenticationException
	 */
	AuthResponseBody postAuthentication()  throws AuthenticationException ;
	/**
	 * This method execute a DELETE request to the Satispay Authentication service
	 * @return the object rappresentation of the service output
	 * @throws AuthenticationException
	 */
	AuthResponseBody deleteAuthentication()  throws AuthenticationException ;

}
