/**
 * 
 */
package com.satispay.sollauth.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.satispay.sollauth.client.Client;
import com.satispay.sollauth.client.DELETEAuthClient;
import com.satispay.sollauth.client.GETAuthClient;
import com.satispay.sollauth.client.POSTAuthClient;
import com.satispay.sollauth.client.PUTAuthClient;
import com.satispay.sollauth.client.request.AuthenticationRequest;
import com.satispay.sollauth.client.request.AuthenticationRequestBody;
import com.satispay.sollauth.client.response.AuthResponseBody;
import com.satispay.sollauth.client.response.AuthenticationResponse;
import com.satispay.sollauth.client.response.Outcome.Status;
import com.satispay.sollauth.exception.AuthenticationException;

import lombok.extern.log4j.Log4j2;

/**
 * @author Andrea Solla
 * @date 10 mag 2021
 */
@Service
@Log4j2
public class AuthServiceImpl implements AuthService{


	@Autowired
	private GETAuthClient getClient;
	@Autowired
	private POSTAuthClient postClient;
	@Autowired
	private PUTAuthClient putClient;
	@Autowired
	private DELETEAuthClient deleteClient;
	
	private static final String ERROR = "An error occurred during authentication";

	public AuthResponseBody getAuthentication() throws AuthenticationException {
		return authentication(getClient);
	}
	
	public AuthResponseBody putAuthentication() throws AuthenticationException {
		return authentication(putClient);
	}
	
	public AuthResponseBody deleteAuthentication() throws AuthenticationException {
		return authentication(deleteClient);
	}
	
	
	public AuthResponseBody postAuthentication() throws AuthenticationException {
		return authentication(postClient);

	}

	private AuthResponseBody authentication(Client<AuthenticationRequest,AuthenticationResponse> client) throws AuthenticationException {
		final AuthenticationResponse response;
		try {
			AuthenticationRequest request = populateRequest();
			response = client.call(request);
		}catch(Exception e) {
			log.error(ERROR,e);
			throw new AuthenticationException(ERROR,e);
		}
		if(response.getOutcome().getStatus()!=Status.OK) {
			throw new AuthenticationException(response.getOutcome().getErrorMessage());
		}
		return response.getBody();
	}
	
	private AuthenticationRequest populateRequest()  {
		AuthenticationRequest request = new AuthenticationRequest();
		request.setBody(new AuthenticationRequestBody());
		request.setHeaders(new HashMap<String, Object>());

		request.getBody().setDummyString("JUST HIRE ME! :)");		


		return request;
	}
}
