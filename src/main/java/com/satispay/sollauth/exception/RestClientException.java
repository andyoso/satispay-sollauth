/**
 * 
 */
package com.satispay.sollauth.exception;

/**
 * @author Andrea Solla
 * @date 11 mag 2021
 */
public class RestClientException extends Exception{

	private static final long serialVersionUID = 1L;

	public RestClientException(String string, Throwable ex) {
		super(string,ex);
	}

	public RestClientException(String string) {
		super(string);
	}
}
