/**
 * 
 */
package com.satispay.sollauth.exception;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
public class SigningException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SigningException(String string, Throwable ex) {
		super(string,ex);
	}
	
	public SigningException(String string) {
		super(string);
	}
}
