/**
 * 
 */
package com.satispay.sollauth.exception;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
public class EncryptionException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public EncryptionException(String string, Throwable ex) {
		super(string,ex);
	}
	
	public EncryptionException(String string) {
		super(string);
	}

}
