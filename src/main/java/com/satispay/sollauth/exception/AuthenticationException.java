/**
 * 
 */
package com.satispay.sollauth.exception;

/**
 * @author Andrea Solla
 * @date 9 mag 2021
 */
public class AuthenticationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthenticationException(String string, Throwable ex) {
		super(string,ex);
	}
	
	public AuthenticationException(String string) {
		super(string);
	}
}
